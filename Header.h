#ifndef HEADER_H
#define HEADER_H


typedef struct komponente {
	char ime[30];
	char proizvođać[30];
	char oznaka[30];
	int  model;
	int cijena;
	int stanje;
}KOMPONENTE;

void unosKomponente();
void pretragaKomponente();
void pregledKomponenti();
int izlaz();

#endif // !HEADER_H

