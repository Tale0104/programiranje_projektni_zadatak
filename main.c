#define _CRT_SECURE_NO_WARNINGS
#include "Header.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


int main() {

	int brKomp = 0;
	int n;
	do {
		printf("\n****SKLADISTE RACUNALNOM OPREMOM\n***");
		printf("\nDobrodosli!\n");
		printf("\nUnesite broj opcije koju zelite koristiti.\n");
		printf("1.\n***Unos komponente***\n");
		printf("2.\n***Pretraga komponente***\n");
		printf("3.\n***Pregled komponenti***\n");
		printf("4.\n***Brisanje komponenti***\n");
		printf("5.\n***Izlaz***\n");
		printf("Odabir opcija: ");
		do {
			scanf("%d", &n);
			if (n < 1 || n>5) {
				printf("Unos je neispravan, molim vas pokusajte ponovo.");
			}
		} while (n < 1 || n>5);
		if (n == 1) {
			unosKomponente();
		}
		else if (n == 2) {
			pretragaKomponente();
		}
		else if (n == 3) {
			pregledKomponenti();
		}
		else if (n == 4) {
			brisanjeKomponente();
		}
		else if (n == 5) {
			izlaz();
		}
	} while (1);
}